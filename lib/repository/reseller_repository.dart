import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:reseller_app_ui_clone/domain/reseller.dart';

abstract class IResellerRepository {
  Future<List<Reseller>> get resellers;
}

class ResellerRepository implements IResellerRepository {
  const ResellerRepository();

  @override
  Future<List<Reseller>> get resellers async {
    final data = await rootBundle.loadStructuredData(
      'assets/dados.json',
      (value) async => jsonDecode(value),
    );

    final resellers = data
        .map((e) => Map<String, dynamic>.from(e))
        .map((e) => Reseller.fromJson(e))
        .cast<Reseller>()
        .toList();

    return resellers;
  }
}
