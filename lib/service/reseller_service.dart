import 'package:reseller_app_ui_clone/domain/reseller.dart';
import 'package:reseller_app_ui_clone/repository/reseller_repository.dart';

abstract class IResellerService {
  Future<List<Reseller>?> get resellers;
}

class ResellerService implements IResellerService {
  const ResellerService(this._resellerRepository);

  final IResellerRepository _resellerRepository;

  @override
  Future<List<Reseller>?> get resellers async {
    try {
      return _resellerRepository.resellers;
    } on Exception {
      return null;
    }
  }
}
