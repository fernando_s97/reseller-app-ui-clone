import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reseller_app_ui_clone/domain/reseller.dart';
import 'package:reseller_app_ui_clone/page/core/top_bar.dart';

class ResellerDetailPageArguments {
  ResellerDetailPageArguments(this.reseller);

  final Reseller reseller;
}

class ResellerDetailPage extends StatefulWidget {
  static const routeName = 'resellerDetail';

  ResellerDetailPage(ResellerDetailPageArguments arguments, {Key? key})
      : _reseller = arguments.reseller,
        super(key: key);

  final Reseller _reseller;

  @override
  _ResellerDetailPageState createState() => _ResellerDetailPageState();
}

class _ResellerDetailPageState extends State<ResellerDetailPage> {
  late int _amount;
  late double _totalPrice;

  @override
  void initState() {
    _amount = 1;
    _updateAmount(_amount);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: 'Selecionar Produtos',
        centerTitle: true,
        showFilterButton: false,
      ),
      body: SafeArea(
        child: Column(
          children: [
            Container(
              color: Colors.white,
              child: Column(
                children: [
                  _StepStatus(),
                  Divider(thickness: 1.5),
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: _Summary(
                      orderAmount: _amount,
                      resellerName: widget._reseller.name,
                      orderPrice: _totalPrice,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                color: Colors.grey.shade200,
                padding: const EdgeInsets.all(16),
                child: Column(
                  children: [
                    _Card(
                      currentGasAmount: _amount,
                      reseller: widget._reseller,
                      onAddClicked: () => _updateAmount(_amount + 1),
                      onRemoveClicked: () => _updateAmount(_amount - 1),
                    ),
                    const Spacer(),
                    _GoToPaymentButton(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _updateAmount(int amount) {
    if (amount <= 0) return;

    final updatedTotalPrice = amount * widget._reseller.price;
    setState(() {
      _amount = amount;
      _totalPrice = updatedTotalPrice;
    });
  }
}

class _StepStatus extends StatelessWidget {
  static const double _dividerSpace = 2.5;

  const _StepStatus({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const divider = Expanded(
      child: Divider(
        indent: _dividerSpace,
        endIndent: _dividerSpace,
      ),
    );

    return Padding(
      padding: const EdgeInsets.all(16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          _Step(name: 'Comprar', isCurrent: true),
          divider,
          _Step(name: 'Pagamento'),
          divider,
          _Step(name: 'Confirmação'),
        ],
      ),
    );
  }
}

class _Step extends StatelessWidget {
  const _Step({Key? key, required String name, bool isCurrent = false})
      : _name = name,
        _isCurrent = isCurrent,
        super(key: key);

  final String _name;
  final bool _isCurrent;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Stack(
          alignment: Alignment.center,
          children: [
            _Circle(
              size: _Circle.defaultCircleSize + 15,
              color: _isCurrent ? Colors.grey.shade300 : Colors.transparent,
            ),
            _Circle(
              size: _Circle.defaultCircleSize,
              border: Border.all(color: Colors.grey),
              color: _isCurrent
                  ? Theme.of(context).primaryColor
                  : Colors.transparent,
            ),
          ],
        ),
        const SizedBox(height: 5),
        Text(_name),
      ],
    );
  }
}

class _Circle extends StatelessWidget {
  static const double defaultCircleSize = 15;

  _Circle({
    Key? key,
    double? size,
    BoxBorder? border,
    Color color = Colors.white,
    Widget? child,
  })  : _size = Size.square(size ?? defaultCircleSize),
        _border = border,
        _color = color,
        _child = child,
        super(key: key);

  final Size _size;
  final BoxBorder? _border;
  final Color _color;
  final Widget? _child;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _size.width,
      height: _size.height,
      decoration: new BoxDecoration(
        border: _border,
        color: _color,
        shape: BoxShape.circle,
      ),
      child: _child,
    );
  }
}

class _Summary extends StatelessWidget {
  const _Summary({
    Key? key,
    required int orderAmount,
    required String resellerName,
    required double orderPrice,
  })  : _orderAmount = orderAmount,
        _resellerName = resellerName,
        _orderPrice = orderPrice,
        super(key: key);

  final int _orderAmount;
  final String _resellerName;
  final double _orderPrice;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _Circle(
          size: 30,
          color: Colors.orange,
          child: Center(
            child: Text(_orderAmount.toString()),
          ),
        ),
        const SizedBox(width: 10),
        Expanded(child: Text('$_resellerName - Botijão de 13kg')),
        _PriceWidget(_orderPrice, bigger: true),
      ],
    );
  }
}

class _PriceWidget extends StatelessWidget {
  const _PriceWidget(double price, {Key? key, bool bigger = false})
      : _price = price,
        _bigger = bigger,
        super(key: key);

  final double _price;
  final bool _bigger;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.baseline,
      textBaseline: TextBaseline.ideographic,
      children: [
        Text(
          'R\$ ',
          style: TextStyle(
            fontSize: (_bigger ? 30 : 25).sp,
          ),
        ),
        TweenAnimationBuilder<double>(
          duration: const Duration(milliseconds: 250),
          tween: Tween(end: _price),
          curve: Curves.linear,
          builder: (_, price, __) {
            final formattedPrice =
                price.toStringAsFixed(2).replaceAll('.', ',');
            return Text(
              formattedPrice,
              style: TextStyle(
                fontSize: (_bigger ? 50 : 45).sp,
                fontWeight: FontWeight.bold,
              ),
            );
          },
        ),
      ],
    );
  }
}

class _Card extends StatelessWidget {
  const _Card({
    Key? key,
    required Reseller reseller,
    required int currentGasAmount,
    required VoidCallback onAddClicked,
    required VoidCallback onRemoveClicked,
  })  : _reseller = reseller,
        _currentGasAmount = currentGasAmount,
        _onAddClicked = onAddClicked,
        _onRemoveClicked = onRemoveClicked,
        super(key: key);

  final Reseller _reseller;
  final int _currentGasAmount;
  final VoidCallback _onAddClicked;
  final VoidCallback _onRemoveClicked;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: 200,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Column(
            children: [
              Expanded(
                flex: 45,
                child: _CardHeader(
                  companyName: _reseller.name,
                  rate: _reseller.rate,
                  duration: _reseller.averageDuration,
                  type: _reseller.type,
                ),
              ),
              const Divider(),
              Expanded(
                flex: 55,
                child: _CardFooter(
                  currentGasAmount: _currentGasAmount,
                  companyName: _reseller.name,
                  price: _reseller.price,
                  onAddClicked: _onAddClicked,
                  onRemoveClicked: _onRemoveClicked,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class _CardHeader extends StatelessWidget {
  const _CardHeader({
    Key? key,
    required String companyName,
    required double rate,
    required String duration,
    required String type,
  })  : _companyName = companyName,
        _rate = rate,
        _duration = duration,
        _type = type,
        super(key: key);

  final String _companyName;
  final double _rate;
  final String _duration;
  final String _type;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Icon(Icons.home, color: Colors.grey, size: 40),
          const SizedBox(width: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(_companyName.toString()),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    _rate.toString(),
                    style: TextStyle(
                      fontSize: 40.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(width: 10),
                  Icon(
                    Icons.star,
                    color: Colors.orange,
                    size: 24,
                  ),
                ],
              ),
            ],
          ),
          const Spacer(),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                padding: const EdgeInsets.all(8),
                color: Colors.black,
                child: Text(
                  _type.toString(),
                  style: TextStyle(color: Colors.white),
                ),
              ),
              // const SizedBox(height: 20),
              Text(
                '$_duration min',
                style: TextStyle(
                  fontSize: 40.sp,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _CardFooter extends StatelessWidget {
  const _CardFooter({
    Key? key,
    required String companyName,
    required double price,
    required int currentGasAmount,
    required VoidCallback onAddClicked,
    required VoidCallback onRemoveClicked,
  })  : _companyName = companyName,
        _price = price,
        _currentGasAmount = currentGasAmount,
        _onAddClicked = onAddClicked,
        _onRemoveClicked = onRemoveClicked,
        super(key: key);

  final String _companyName;
  final double _price;
  final int _currentGasAmount;
  final VoidCallback _onAddClicked;
  final VoidCallback _onRemoveClicked;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Botijão de 13kg'),
                Text(_companyName.toString()),
                _PriceWidget(_price),
              ],
            ),
          ),
          _GasAmountPicker(
            currentGasAmount: _currentGasAmount,
            onAddClicked: _onAddClicked,
            onRemoveClicked: _onRemoveClicked,
          ),
        ],
      ),
    );
  }
}

class _GasAmountPicker extends StatelessWidget {
  final _buttonsColor = Colors.grey.shade300;

  _GasAmountPicker({
    Key? key,
    required int currentGasAmount,
    required VoidCallback onAddClicked,
    required VoidCallback onRemoveClicked,
  })  : _currentGasAmount = currentGasAmount,
        _onAddClicked = onAddClicked,
        _onRemoveClicked = onRemoveClicked,
        super(key: key);

  final int _currentGasAmount;
  final VoidCallback _onAddClicked;
  final VoidCallback _onRemoveClicked;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        InkWell(
          onTap: _onRemoveClicked,
          child: _Circle(
            color: _buttonsColor,
            size: 30,
            child: Icon(Icons.remove),
          ),
        ),
        const SizedBox(width: 10),
        Stack(
          alignment: Alignment.center,
          children: [
            Image.asset(
              'assets/images/gas_canister.png',
              color: Colors.orange,
              width: 50,
              height: 50,
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 4),
              margin: const EdgeInsets.only(top: 9),
              decoration: BoxDecoration(
                color: Colors.orange,
                borderRadius: BorderRadius.circular(30),
              ),
              child: Text(_currentGasAmount.toString()),
            )
          ],
        ),
        const SizedBox(width: 10),
        InkWell(
          onTap: _onAddClicked,
          child: _Circle(
            color: _buttonsColor,
            size: 30,
            child: Icon(Icons.add),
          ),
        ),
      ],
    );
  }
}

class _GoToPaymentButton extends StatelessWidget {
  const _GoToPaymentButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final buttonColor = Theme.of(context).primaryColor;

    return Container(
      padding: const EdgeInsets.all(25),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(60),
        gradient: LinearGradient(
          colors: [.6, 1.0].map(buttonColor.withOpacity).toList(),
        ),
      ),
      child: const Text(
        'IR PARA O PAGAMENTO',
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
      ),
    );
  }
}
