import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reseller_app_ui_clone/domain/reseller.dart';
import 'package:reseller_app_ui_clone/page/core/top_bar.dart';
import 'package:reseller_app_ui_clone/page/resellers/reseller_card.dart';
import 'package:reseller_app_ui_clone/page/reseller_detail/reseller_detail_page.dart';
import 'package:reseller_app_ui_clone/service/reseller_service.dart';

class ResellersPage extends StatelessWidget {
  const ResellersPage({Key? key, required IResellerService resellerService})
      : _resellerService = resellerService,
        super(key: key);

  final IResellerService _resellerService;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TopBar(
        title: 'Escolha uma Revenda',
        centerTitle: false,
        showFilterButton: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            _Header(),
            Expanded(
              child: _Resellers(resellerService: _resellerService),
            ),
          ],
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(16),
      color: Colors.white,
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Botijões de 13kg em:',
                  style: TextStyle(fontSize: 22.sp, color: Colors.grey),
                  overflow: TextOverflow.ellipsis,
                ),
                const Text(
                  'Av Paulista, 1001',
                  overflow: TextOverflow.ellipsis,
                ),
                const Text(
                  'Paulista, São Paulo, SP',
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.grey),
                ),
              ],
            ),
          ),
          Column(
            children: [
              Icon(
                Icons.location_pin,
                color: Theme.of(context).primaryColor,
              ),
              Text(
                'Mudar',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Theme.of(context).primaryColor),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class _Resellers extends StatelessWidget {
  const _Resellers({
    Key? key,
    required IResellerService resellerService,
  })  : _resellerService = resellerService,
        super(key: key);

  final IResellerService _resellerService;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey.shade200,
      child: Center(
        child: FutureBuilder<List<Reseller>?>(
          future: _resellerService.resellers,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return _ResellersLoaded(resellers: snapshot.data!);
            }

            if (snapshot.hasError) return _ResellersLoadError();

            return _ResellersLoading();
          },
        ),
      ),
    );
  }
}

class _ResellersLoading extends StatelessWidget {
  const _ResellersLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: const [
          CircularProgressIndicator(),
          SizedBox(height: 10),
          Text('Carregando'),
        ],
      ),
    );
  }
}

class _ResellersLoadError extends StatelessWidget {
  const _ResellersLoadError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text('Não possível carregar os revendedores');
  }
}

class _ResellersLoaded extends StatelessWidget {
  const _ResellersLoaded({
    Key? key,
    required List<Reseller> resellers,
  })  : _resellers = resellers,
        super(key: key);

  final List<Reseller> _resellers;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
      itemBuilder: (context, index) {
        final reseller = _resellers[index];
        return ResellerCard(
          reseller: reseller,
          onTap: () => Navigator.of(context).pushNamed(
            ResellerDetailPage.routeName,
            arguments: ResellerDetailPageArguments(reseller),
          ),
        );
      },
      separatorBuilder: (context, index) => const SizedBox(height: 25),
      itemCount: _resellers.length,
    );
  }
}
