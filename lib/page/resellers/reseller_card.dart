import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reseller_app_ui_clone/core/extensions.dart';
import 'package:reseller_app_ui_clone/domain/reseller.dart';

class ResellerCard extends StatelessWidget {
  const ResellerCard({Key? key, required Reseller reseller, VoidCallback? onTap})
      : _reseller = reseller,
        _onTap = onTap,
        super(key: key);

  final Reseller _reseller;
  final VoidCallback? _onTap;

  @override
  Widget build(BuildContext context) {
    final borderRadius = BorderRadius.circular(8);

    return ClipRRect(
      borderRadius: borderRadius,
      child: InkWell(
        onTap: _onTap,
        child: Container(
          decoration: BoxDecoration(color: Colors.white),
          height: 130,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              _Label(
                label: _reseller.type,
                backgroundColor: ColorsExtension.fromHex(_reseller.color),
              ),
              Expanded(child: _Content(_reseller)),
            ],
          ),
        ),
      ),
    );
  }
}

class _Label extends StatelessWidget {
  const _Label({
    Key? key,
    required String label,
    required Color backgroundColor,
  })  : _label = label,
        _backgroundColor = backgroundColor,
        super(key: key);

  final String _label;
  final Color _backgroundColor;

  @override
  Widget build(BuildContext context) {
    return RotatedBox(
      quarterTurns: -1,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 12),
        color: _backgroundColor,
        child: Center(
          child: Text(
            _label,
            style: TextStyle(color: Colors.white),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ),
    );
  }
}

class _Content extends StatelessWidget {
  static const double padding = 10;

  const _Content(Reseller reseller, {Key? key})
      : _reseller = reseller,
        super(key: key);

  final Reseller _reseller;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:
          const EdgeInsets.symmetric(vertical: padding).copyWith(left: padding),
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 40,
            child: _Header(reseller: _reseller),
          ),
          Expanded(
            flex: 60,
            child: _Footer(reseller: _reseller),
          ),
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header({
    Key? key,
    required Reseller reseller,
  })  : _reseller = reseller,
        super(key: key);

  final Reseller _reseller;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Text(
            _reseller.name,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        if (_reseller.isBestPrice) _BestPriceLabel(),
      ],
    );
  }
}

class _BestPriceLabel extends StatelessWidget {
  const _BestPriceLabel({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final borderRadius = Radius.circular(5);

    return Container(
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.orange,
        borderRadius: BorderRadius.only(
          topLeft: borderRadius,
          bottomLeft: borderRadius,
        ),
      ),
      child: SingleChildScrollView(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Icon(Icons.label, color: Colors.white),
            const SizedBox(width: 5),
            Text(
              'Melhor Preço',
              style: TextStyle(color: Colors.white),
            ),
          ],
        ),
      ),
    );
  }
}

class _Footer extends StatelessWidget {
  static final double titleFontSize = 25.sp;
  static final double contentFontSize = 50.sp;
  static const double titleContentSpaceSize = 5;

  const _Footer({Key? key, required Reseller reseller})
      : _reseller = reseller,
        super(key: key);

  final Reseller _reseller;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(right: _Content.padding),
      child: Center(
        child: SingleChildScrollView(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _Rate(rate: _reseller.rate),
              _AverageDuration(range: _reseller.averageDuration),
              _Price(price: _reseller.price),
            ],
          ),
        ),
      ),
    );
  }
}

class _Rate extends StatelessWidget {
  const _Rate({
    Key? key,
    required double rate,
  })  : _rate = rate,
        super(key: key);

  final double _rate;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Nota',
          style: TextStyle(
            color: Colors.grey,
            fontSize: _Footer.titleFontSize,
          ),
        ),
        const SizedBox(height: _Footer.titleContentSpaceSize),
        Row(
          children: [
            Text(
              _rate.toString(),
              style: TextStyle(
                fontSize: _Footer.contentFontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
            Icon(
              Icons.star,
              color: Colors.yellow,
            ),
          ],
        ),
      ],
    );
  }
}

class _AverageDuration extends StatelessWidget {
  const _AverageDuration({
    Key? key,
    required String range,
  })  : _duration = range,
        super(key: key);

  final String _duration;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Tempo Médio',
          style: TextStyle(
            color: Colors.grey,
            fontSize: _Footer.titleFontSize,
          ),
        ),
        const SizedBox(height: _Footer.titleContentSpaceSize),
        Row(
          crossAxisAlignment: CrossAxisAlignment.baseline,
          textBaseline: TextBaseline.ideographic,
          children: [
            Text(
              _duration,
              style: TextStyle(
                fontSize: _Footer.contentFontSize,
                fontWeight: FontWeight.bold,
              ),
            ),
            Text(
              'min',
              style: TextStyle(
                fontSize: _Footer.titleFontSize,
                color: Colors.grey,
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class _Price extends StatelessWidget {
  const _Price({
    Key? key,
    required double price,
  })  : _price = price,
        super(key: key);

  final double _price;

  @override
  Widget build(BuildContext context) {
    final formattedPrice = _price.toStringAsFixed(2).replaceAll('.', ',');

    return Column(
      children: [
        Text(
          'Preço',
          style: TextStyle(
            color: Colors.grey,
            fontSize: _Footer.titleFontSize,
          ),
        ),
        const SizedBox(height: _Footer.titleContentSpaceSize),
        Text(
          'R\$ $formattedPrice',
          style: TextStyle(
            fontSize: _Footer.contentFontSize,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
