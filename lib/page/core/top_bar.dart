import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class TopBar extends AppBar {
  TopBar({
    Key? key,
    required String title,
    required bool centerTitle,
    required bool showFilterButton,
  }) : super(
          key: key,
          title: Text(title, overflow: TextOverflow.ellipsis),
          centerTitle: centerTitle,
          actions: [
            if (showFilterButton) _FilterMenuButton(),
            if (showFilterButton) const SizedBox(width: 5),
            _HelpMenuButton(),
            const SizedBox(width: 15),
          ],
        );
}

class _FilterMenuButton extends StatelessWidget {
  const _FilterMenuButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final itemColor = Theme.of(context).primaryColor;

    return PopupMenuButton(
      child: Icon(Icons.import_export),
      itemBuilder: (context) => [
        'Melhor avaliação',
        'Mais rápido',
        'Mais barato',
      ].map((item) {
        return PopupMenuItem(
          child: CheckboxListTile(
            value: false,
            onChanged: (_) {},
            title: Text(item, style: TextStyle(color: itemColor)),
          ),
        );
      }).toList(),
    );
  }
}

class _HelpMenuButton extends StatelessWidget {
  const _HelpMenuButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final itemColor = Theme.of(context).primaryColor;

    return PopupMenuButton(
      child: Center(child: Text('?', style: TextStyle(fontSize: 50.sp))),
      itemBuilder: (context) => ['Suporte', 'Termos de serviço'].map((item) {
        return PopupMenuItem(
          child: CheckboxListTile(
            value: false,
            onChanged: (_) {},
            title: Text(item, style: TextStyle(color: itemColor)),
          ),
        );
      }).toList(),
    );
  }
}
