import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:reseller_app_ui_clone/page/resellers/resellers_page.dart';
import 'package:reseller_app_ui_clone/page/reseller_detail/reseller_detail_page.dart';
import 'package:reseller_app_ui_clone/repository/reseller_repository.dart';
import 'package:reseller_app_ui_clone/service/reseller_service.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
  ]);

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static const IResellerService _resellerService =
      ResellerService(ResellerRepository());

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: Size(768, 1280),
      builder: () => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ResellersPage(resellerService: _resellerService),
        routes: {
          ResellerDetailPage.routeName: (context) {
            final arguments = ModalRoute.of(context)!.settings.arguments
                as ResellerDetailPageArguments;

            return ResellerDetailPage(arguments);
          }
        },
      ),
    );
  }
}
