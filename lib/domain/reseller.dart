import 'package:flutter/cupertino.dart';

@immutable
class Reseller {
  final String type;
  final String name;
  final String color;
  final double rate;
  final String averageDuration;
  final bool isBestPrice;
  final double price;

  const Reseller({
    required this.type,
    required this.name,
    required this.color,
    required this.rate,
    required this.averageDuration,
    required this.isBestPrice,
    required this.price,
  });

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other is Reseller &&
            type == other.type &&
            name == other.name &&
            color == other.color &&
            rate == other.rate &&
            averageDuration == other.averageDuration &&
            isBestPrice == other.isBestPrice &&
            price == other.price);
  }

  @override
  int get hashCode =>
      type.hashCode ^
      name.hashCode ^
      color.hashCode ^
      rate.hashCode ^
      averageDuration.hashCode ^
      isBestPrice.hashCode ^
      price.hashCode;

  @override
  String toString() => 'Reseller('
      'type: $type, '
      'name: $name, '
      'color: $color, '
      'rate: $rate, '
      'averageDuration: $averageDuration, '
      'isBestPrice: $isBestPrice, '
      'price: $price'
      ')';

  Reseller.fromJson(Map<String, dynamic> json)
      : type = json['tipo'],
        name = json['nome'],
        color = json['cor'],
        rate = json['nota'],
        averageDuration = json['tempoMedio'],
        isBestPrice = json['melhorPreco'],
        price = json['preco'];

  Map<String, dynamic> toJson() => {
        'tipo': type,
        'nome': name,
        'cor': color,
        'nota': rate,
        'tempoMedio': averageDuration,
        'melhorPreco': isBestPrice,
        'preco': price
      };
}
