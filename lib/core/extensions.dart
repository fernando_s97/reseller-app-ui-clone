import 'package:flutter/material.dart';

extension ColorsExtension on Color {
  // https://dev.to/alexandrefreire/hexadecimal-color-strings-in-flutter-57b1
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}